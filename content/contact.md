---
title: Contact
---

# Contact

Vous souhaitez…
- me poser une question : je réponds volontiers à vos questions ! Si vous ayez besoin d’aide pour installer et utiliser un logiciel ou si vous n’avez pas compris l’une de mes explications, n’hésitez pas à me le faire savoir. Je vous répondrais du mieux que je peux ou vous redirigerais vers d’autres personnes si c’est hors de mon domaine de compétence
- me faire part d’une suggestion : je suis ouvert à vos diverses suggestions pour améliorer le site ! Si vous avez un sujet sur lequel vous voudriez en savoir plus ou un besoin particulier (lié à l’informatique et/ou au libre), dites-le moi et ça pourrais faire le sujet d’un prochain article.
- signaler une erreur dans un article ou un problème sur le site: merci d’avance de bien préciser de quel article il s’agit (url de l’article et titre du paragraphe en question, par exemple) afin que je puisse corriger le problème au plus vite
- simplement me laisser un petit mot ou dire bonjour

Contactez moi par mail [rabbitstemplate@disroot.org](mailto:rabbitstemplate@disroot.org) ou sur matrix [@rhombihexahedron:matrix.org](https://matrix.to/#/@rhombihexahedron:matrix.org)
