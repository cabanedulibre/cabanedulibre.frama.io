---
title: Le libre, c’est quoi ?
date: 2023-04-10
---

## D’où l’on part

Chaque logiciel que vous utilisez est soumis à une licence, un contrat légal que vous êtes tenu d’accepter afin d’utiliser le logiciel.
Les logiciels privateurs (ceux des [GAFAM](https://fr.wikipedia.org/wiki/GAFAM) notamment) comportent souvent dans leur licence des clauses qui restreignent les droits des utilisateurs et donnent aux entreprises qui les conçoivent un très grand pouvoir, qui peut être utilisé de manière abusive.

Quelques exemples:
- Microsoft fait en sorte qu’il soit [très difficile de changer le navigateur par défaut](https://www.theverge.com/22630319/microsoft-windows-11-default-browser-changes) sur windows pour pousser les utilisateurs à utiliser leurs produits plutôt que ceux de la concurrence   
- Apple conçoit ces produits en sorte qu’ils soient [incompatible avec ceux de la concurrence](https://www.theguardian.com/technology/2021/may/30/gadgets-have-stopped-working-together-interoperability-apple) (prises non standard, formats de fichier non documentés…). Ces utilisateurs sont ainsi bloqués dans le système d’apple. 
- Klim, une entreprise vends des vestes-airbag pour les motards, avec un système d’abonnement. Si l’utilisateur oublie de renouveler l’abonnement, [la veste est désactivée à distance et ne s’activera pas en cas d’accident](https://www.bleepingcomputer.com/news/legal/canon-sued-for-disabling-scanner-when-printers-run-out-of-ink/)

> Heureusement, la loi est là pour nous protéger, non ?

En effet, la loi doit protéger les individus des abus en tout genre, y compris dans le domaine du numérique. Cependant, même si des progrès ont étés faits, de mon point de vue, la législation reste en pratique très "en retard" par rapport aux avancées technologiques, et ce pour de multiples raisons. L’une de ces raisons, à mon avis, est que les législateurs (et la population en général) ont une compréhension trop faible du fonctionnement et des enjeux des outils numérique. Je crois même que de nombreux problèmes actuels liés au numérique pourraient être résolus en informant correctement les utilisateurs, sans avoir besoin de créer la moindre loi.

## Le code, c’est la loi

Un point important qu’il faut bien comprendre à propos des outils numérique:
**le code, c’est la loi**  
Je parle ici du code source, du code informatique qui fait fonctionner les diverses applications et outils numérique, pas du code pénal. Dans le monde informatique, c’est le programmeur qui décide ce qu’il est possible de faire ou ne pas faire, c’est lui et lui seul qui écrit les règles du jeu (et pas un quelconque législateur). Celui qui contrôle les programmeurs contrôle donc le monde numérique et peut décider des règles. Il est alors tentant pour cette personne de tourner les règles à son avantage, au détriment des utilisateurs finaux.

> Hum… Mais alors, pour éviter les abus, il suffirait d’avoir une personne qui connaisse à la foi la loi et qui sache lire du code informatique et de lui demander de vérifier si le code informatique d’un logiciel correspond bien à ce qui est permis par la loi. On serait alors certain que les règles du jeu qui s’appliquent en pratique ne font de tord à personne injustement.

C’est une bonne idée, mais pour que cette vérification puisse se faire, il est nécessaire d’avoir le code source du logiciel. Or, ce n’est actuellement pas le cas pour un très grand nombre de logiciels.

## Code source et code compilé

Il existe deux sortes de code informatique: le code source et le code compilé. Pour expliquer la différence entre ces deux sortes de codes, je vais commencer par une analogie culinaire.

Imaginez un pâtissier. Le pâtissier peut inventer différentes recettes de gâteaux, comme la tarte aux pommes. Il y a une différence importante entre la recette de la tarte aux pommes et le gâteau « tarte aux pommes » tout chaud qui sort du four. Si vous voulez vérifier ce que contient le gâteau (est-ce qu’il y a de la cannelle, quel est la quantité de sucre…) et que vous avez la recette, c’est facile. Il suffit de lire la liste des ingrédients et vous saurez exactement ce qu’il contient. Si vous avez seulement le gâteau cuit, c’est pratiquement impossible (vous pouvez y goûter mais bon courage pour déterminer la quantité exacte de chaque ingrédient).

En informatique, c’est très similaire. Le code source est la recette de cuisine. Il peut ensuite être transformé en un code compilé (le gâteau cuit). Avec seulement le code compilé, le programme peut être utilisé (le gâteau peut être mangé) mais il est très difficile de vérifier ce que fait exactement le programme (connaître les ingrédients et les étapes de fabrication).

> Je comprends, pour pouvoir vérifier ce que fait un programme, il faut avoir accès à la recette et pas seulement au résultat final. Et ça existe, des programmes dont on a la recette ?

## Les logiciels open source (logiciels à source ouverte)

Un logiciel open source est un logiciel dont on dispose du code source, de la recette de fabrication. On peut donc vérifier ce que fait ce logiciel et s’assurer qu’il fait bien ce qu’on souhaite qu’il fasse et rien de plus (en particulier, rien de malveillant en plus).

Cependant, on ne peut pas modifier le logiciel, on peut seulement vérifier ce qu’il fait et l’utiliser ou non.

> Ah bon ? Mais tu viens pourtant de dire qu’on a le code source, donc la recette de ces logiciels. Qu’est ce qui m’empêche de modifier la recette comme je veux ?

D’un point de vue purement technique, tu peux en effet modifier assez facilement un logiciel dont tu possèdes le code source. Le seul soucis, c’est que ce n’est probablement pas légal. À moins que le créateur du logiciel t’y autorises, tu n’as pas le droit de modifier son logiciel, encore moins de distribuer à d’autres ces versions modifiées de son logiciel. Avec un logiciel seulement open source, tu peux lire la recette et utiliser le logiciel tel quel, c’est tout. C’est à prendre ou à laisser.
Avoir accès au code source permet aux utilisateurs de vérifier ce que fait un logiciel mais pas d’en avoir le contrôle.

## Logiciel libre

Les [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre) sont une catégorie particulière de logiciels open source. Ces logiciels placent la liberté de l’utilisateur au centre de leur conception. Leur licence donne à l’utilisateur le droit:
- d’utiliser le logiciel comme il veut, quand il veut
- d’étudier le fonctionnement du logiciel (open source)
- d’améliorer le logiciel
- de redistribuer le logiciel à d’autres personnes, dans sa forme originale ou une version améliorée (pour en faire profiter tout le monde)

Ainsi, en plus d’être vérifiables, ces logiciels sont aussi modifiables par les utilisateurs. Le contrôle reste dans les mains des utilisateurs. 

## La culture du libre

La philosophie du logiciel libre s’est étendu à d’autres domaines et forme ainsi ce que l’on nomme la « culture du libre ». Par exemple, certains artistes vont placer leur œuvre (roman, bande dessinée, musique…) sous une licence libre, ce qui permet à d’autres de la réutiliser pour créer de nouvelles œuvres.

Exemples: 
- un vidéaste peut utiliser une musique sous licence libre pour accompagner sa dernière vidéo

- un créateur de jeux peut utiliser les personnages d’une bande dessinée libre pour les intégrer à son jeu de société

- les fans d’un roman sous licence libre peuvent inventer une suite à l’histoire et la publier

> Si tout le monde peut utiliser une œuvre comme il veut, ça veut dire que le libre c’est gratuit ? Comment sont payés les créateurs d’œuvres libres ?

## Le libre, c’est gratuit ?

Les licences libres ne spécifient rien de particulier à propos de l’aspect financier.
Un logiciel ou une œuvre sous licence libre peut être utilisée à des fins commerciales/pour gagner de l’argent. À l’inverse, un logiciel privateur peut tout à fait être gratuit.

Dans la pratique, de nombreux développeurs de logiciel libre font le choix de distribuer leur logiciel gratuitement, avec parfois un système de don qui permet de soutenir le projet. D’autres modèles économiques sont bien sûr possible, la philosophie du libre n’impose rien à ce sujet.


## En résumé

Le libre est une éthique centrée sur le respect de la liberté de chacun et le partage de la connaissance pour le bien commun.

Les œuvres intellectuelles qui peuvent être dupliqué à l’infini (logiciels, textes, images, musiques, vidéos… sous forme numérique) sont considérées comme un bien commun qui doit pouvoir être distribué et modifié librement. Dans le cas des logiciels libres en particulier, les utilisateurs ont la possibilité technique et légale de contrôler le logiciel, ce qui empêche les abus de pouvoir potentiels de la part du concepteur du logiciel et garanti une vraie liberté aux utilisateurs. 
