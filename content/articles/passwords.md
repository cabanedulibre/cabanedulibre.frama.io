---
title: Les mots de passe
date: 2023-04-11
---

Ahhh, les mots de passe ! À chaque fois que vous devez vous inscrire sur un site quelconque, c’est la même histoire. On vous demande de choisir un mot de passe. Un mot de passe de plus à ajouter à la liste (souvent déjà longue) de mots de passe que vous devez déjà retenir.
Pour éviter cette charge mentale, la tentation est d’utiliser des mots de passe "simple" à retenir comme votre prénom, votre date de naissance ou le titre de votre livre favori… ce qui réduit malheureusement la sécurité de votre compte. 
Comment faire pour avoir des mots de passe à la fois sécurisés et faciles à retenir ? Et d’ailleurs, ça sert à quoi les mots de passe ?

## Petit tour d’horizon de l’univers des mots de passe

### À quoi servent les mots de passe

Pour comprendre à quoi servent les mots de passe, prenons l’exemple d’une banque. Avant l’arrivée de l’informatique, pour effectuer une opération sur votre compte bancaire, disons retirer de l’argent, vous deviez vous rendre au guichet de votre banque et demander la somme que vous souhaitez retirer. Avant de vous donner l’argent, le personnel de la banque va vérifier que vous êtes bien le propriétaire du compte. Cette étape de contrôle permet d’empêcher que quelqu’un d’autre que vous puisse retirer de l’argent sur votre compte. Pour cela, on vous demande votre carte d’identité. Si le nom inscrit sur la carte correspond bien au nom du propriétaire du compte et si votre tête correspond à peu près à la photo et l’âge inscrit sur la carte, vous pouvez avoir votre argent. La sécurité de l’opération réside dans le fait que vous et vous seul possédez une telle carte d’identité à votre nom.
Si un attaquant voulait contourner cette sécurité, il faudrait qu’il fabrique une fausse carte d’identité à votre nom et se présente à votre banque.
Plutôt difficile mais pas impossible.
(je simplifie évidemment mais c’est l’idée générale qui compte, dans la pratique, les banques ont des systèmes de contrôle plus sophistiqués)

À présent, l’informatique à fait son apparition et vous souhaitez accéder à votre compte en ligne. Dans le monde informatique, on a pas de carte d’identité numérique. À sa place, on utilise un mot de passe, qui joue un rôle similaire. Le mot de passe est un secret connu seulement de vous et de la banque. Quand vous vous connectez à votre compte, le fait de fournir le bon mot de passe prouve à la banque qu’il s’agit bien de vous.
Cette fois, pour contourner cette sécurité et siphonner votre compte bancaire, l’attaquant n’a pas d’autre possibilités que d’obtenir ce fameux mot de passe. Comme l’attaquant ne peut pas lire le mot de passe dans vos pensées, il lui faut une autre technique…

### Techniques de malfaiteurs pour obtenir votre mot de passe

- **deviner votre mot de passe**: c’est assez facile à faire si vous utilisez votre nom ou votre date de naissance comme mot de passe

- **l’attaque dictionnaire**: l’attaquant fait un petit programme qui essaie de se connecter à votre compte en testant différents mots de passe les uns après les autres. Ces mots de passe testés sont pris dans une grande liste qui contient à la fois des mots du dictionnaire (d’où le nom donné à ce genre d’attaques) mais aussi des tas de mots de passe courants (comme "azerty", "1234" ou "admin").

- **la force brute**: si les deux méthodes précédentes ont échouées, l’attaquant peut recourir à la force brute. Cette méthode consiste à tester purement et simplement tous les mots de passe possible. Théoriquement, cette méthode permet de craquer n’importe quel mot de passe, à condition d’attendre suffisamment longtemps. La seule chose qui vous protège contre ce type d’attaque, c’est d’utiliser un mot de passe suffisamment long. 

- **l’hameçonnage** (phishing en anglais): une technique assez différente des trois autres. Ici, l’attaquant vous envoie un mail ou un sms en se faisant passer pour votre banque afin de vous soutirer des renseignements personnels tels que votre mot de passe. Dans la suite de cet article, je vais ignorer cette méthode, qui mériterais bien un article entier dans le futur. 

Pour résister à ces différentes attaques, il nous faut donc un mot de passe difficile à deviner, qui ne soit ni un mot du dictionnaire ni un mot de passe courant et qui soit assez long pour résister aux attaques par force brute.


### Des mots de passe solides

Il va nous falloir des mots de passe solides. Oui, je dis bien « des » parce qu’il nous faut un mot de passe différent sur chaque site. C’est une mauvaise idée d’utiliser le même mot de passe sur plusieurs sites différents. Sans entrer trop dans les détails, s’il y a une fuite de données sur un site, les cybercriminels vont s’empresser de tester les mots de passe obtenus sur d’autres sites. Ainsi, si vous utilisez le même mot de passe pour tous vos comptes, il suffit qu’une faille de sécurité soit découverte sur l’un des sites où vous avez un compte et c’est l’ensemble de vos comptes qui sont compromis d’un seul coup !

Nous disions donc, des mots de passe solides. Qu’est-ce qui fait qu’un mot de passe est solide ?

Cassons tous de suite une mauvaise conception répandue: oubliez ces histoires d’avoir des majuscules, des chiffres ou des caractères spéciaux dans vos mots de passe. Ça les rends plus difficile à retenir pour vous et pas beaucoup plus robustes. Ce qui compte vraiment, c’est la longueur du mot de passe.
Petit exemple comparatif pour vous en rendre compte: 
- un mot de passe de 7 caractères avec des lettres majuscules et minuscules, des chiffres et des caractères spéciaux se craque en moins de 10 minutes
- un mot de passe de 16 caractères avec seulement des lettres minuscules prendrait 34 000 ans à être craqué

Pour ceux que ça intéresse, [ce site](https://www.security.org/how-secure-is-my-password/) permet de voir combien de temps un mot de passe mettrait à être craqué.

Si on résume, il nous faut un mot de passe différent pour chaque site et chaque mot de passe doit être assez long pour être suffisamment robuste aux attaques potentielles. Bonjour la charge mentale pour les retenir me direz vous. Ce serait quand même plus simple si on pouvait au moins utiliser le même mot de passe partout…

Et bien en fait, c’est possible.

## Gérer ses mots de passe avec un générateur de mots de passe

Pour nous simplifier la vie, on peut utiliser un générateur de mots de passe tel que [lesspass](https://www.lesspass.com/).

Il s’agit d’une petite extension que vous installez sur votre navigateur et qui génère les mots de passe pour vous chaque fois que vous vous connectez à un site.

### Comment ça fonctionne

Chaque fois que vous vous connectez à un site, vous entrez plusieurs informations dans la petite fenêtre lesspass tel que votre nom d’utilisateur et votre mot de passe maître (master password).
Lesspass va ensuite générer un mot de passe à partir des informations fournies.
La prochaine fois que vous vous connectez au site, il vous suffit de rentrer les mêmes informations et lesspass génère à nouveau le même mot de passe.

**Avantages:**
- vous n’avez besoin de retenir qu’un seul mot de passe, le mot de passe maître. Un seul mot de passe à retenir pour tous les sites, la charge mentale est réduite au minimum.
- lesspass va générer un mot de passe différent et robuste pour chaque site, vous bénéficiez ainsi d’une sécurité au top
- lesspass n’enregistre pas vos mots de passe (au contraire des autres gestionnaires de mots de passe), il les génère à chaque fois à partir des information que vous lui fournissez. Ça signifie que vous n’avez pas de risque de perdre ou de vous faire voler votre base de donnée de mots de passe (puisqu’il n’y en a pas). Pas de problèmes de synchronisation non plus, il vous suffit d’installer l’extension ou d’aller sur le site de lesspass pour générer vos mots de passe sur n’importe quel ordinateur.
- c’est gratuit, libre et open source

### Installation

Pour installer lesspass :
- rendez-vous sur [lesspass.com](https://www.lesspass.com/)
- descendez plus bas dans la page et choisissez la version qu’il vous faut en cliquant sur le logo qui correspond à votre appareil / navigateur (vous devriez voir quelque chose qui ressemble à l’image ci-dessous)

![Étapes montrant comment installer l’extension navigateur lesspass](/images/lesspass_install_platforms.png)

- suivez ensuite les étapes pour l’installation. Ci-dessous, un exemple avec les étapes à suivre pour l’installation sur firefox

![Étapes montrant comment installer l’extension lesspass sur firefox](/images/lesspass_install_firefox.png)

- une fois installé, vous pouvez ajouter lesspass à votre barre d’outils

![Étapes montrant comment ajouter l’extension lesspass dans la barre d’outils sur firefox](/images/add_lesspass_to_toolbar.png)

### Utilisation

Pour vous connecter à un site, voici la procédure :
- **saisissez votre nom d’utilisateur** sur la page de connexion du site
- **lancez lesspass** (cliquez sur le logo bleu avec une clef dans la barre d’outil ou pressez CTRL-MAJ-L)
- une petite fenêtre s’ouvre, vous **entrez votre identifiant et votre mot de passe maître**
- **cliquez sur "Generate & Copy"** pour générer votre mot de passe (le mot de passe généré est copié dans le presse-papier)
- **collez le mot de passe généré** dans le champ destiné au mot de passe de la page de connexion du site 
- cliquez sur le bouton de connexion

Lorsque vous créez un compte sur un site (ou mettez à jour votre mot de passe sur l’un de vos comptes), la procédure est identique. Il suffit simplement de coller le mot de passe généré dans le champ servant à définir votre nouveau mot de passe.

![Capture d’écran de la fenêtre d’interface lesspass](/images/lesspass_window_filled.png)


### Choisir son mot de passe maître

Vous n’aurez plus qu’un seul mot de passe à mémoriser, il convient donc de le choisir avec soin.
On l’a vu précédemment, choisir un mot de passe robuste n’est pas une tâche aisée.
Une méthode qui permet de générer un mot de passe robuste et facile à retenir est la méthode [diceware](https://diceware.fr/).

Cette méthode consiste à tirer des mots au hasard du dictionnaire. Votre mot de passe (ou plutôt phrase de passe en l’occurrence) sera cette suite de mots. La phrase obtenue forme souvent une image étrange, une petite histoire amusante, ce qui la rends facile à retenir. De plus, comme il s’agit de votre mot de passe maître, vous l’utiliserez très souvent donc aucun risque de l’oublier.

Un exemple ? Je suis allé sur [diceware.fr](https://diceware.fr/) et j’ai tiré 6 mots au hasard.
Les 6 mots sont exode, étude, hoquet, jambe, moment et larme. Ma phrase de passe serait alors "exode étude hoquet jambe moment larme".

## Conclusion

J’espère que ce petit guide vous aura appris quelques petites choses sur les mots de passe et vous sera utile dans vos usages.

Si vous avez des commentaires, remarques, questions, demandes, je suis joignable par divers canaux (cf section contact)

## Sources et autres liens utiles

Détails technique sur le fonctionnement de lesspass (en anglais) :  
https://blog.lesspass.com/2016-10-19/how-does-it-work

Sur la méthode diceware et les mots de passe :  
https://lecrabeinfo.net/choisir-un-bon-mot-de-passe-fort-et-securise-avec-diceware.html

Sur les attaques brute-force de mots de passe (en anglais) :  
https://irontechsecurity.com/how-long-does-it-take-a-hacker-to-brute-force-a-password/
