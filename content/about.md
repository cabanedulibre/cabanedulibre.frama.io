---
title: À propos
---

# À propos de « la cabane du libre »

## Pourquoi ce site 

En quelques décennies, l’usage de l’outil numérique s’est répandu dans tous les domaines de notre société. Ainsi, nos institutions et entreprises sont devenues dépendantes du bon fonctionnement de cet outil, pour le meilleur comme pour le pire.

Cependant, alors que l’usage de l’outil informatique s’est généralisé, sa compréhension par le grand public reste insuffisante. Certaines entreprises du numérique exploitent parfois cette ignorance pour leur propre intérêt, quitte à faire passer les considérations éthiques au second plan.

Heureusement, il existe des logiciels alternatifs plus respectueux des utilisateurs: [les logiciels libres](/articles/univers_du_libre). 
Cependant, ces alternatives ne disposent pas de millions de budget pour leur communication et sont par conséquent peu connues du public.

Ce site souhaite, à sa manière, contribuer à rendre le monde numérique plus sain.
Concrètement, ses objectifs sont:
- expliquer de manière claire et simple le fonctionnement des outils informatiques et leurs enjeux
- faire découvrir les logiciels libres – et plus largement la culture du libre –
- fournir des guides pratiques pour libérer vos usages numériques

## L’auteur

Bonjour ! Je m’appelle Baptiste Lambert, je suis étudiant en informatique. Je passe donc une bonne partie de mes journées à programmer des choses plus ou moins utiles. Quand je ne suis pas en train de coder, j’aime cuisiner des petits plats, doubler des files de voitures à vélo ou apprendre des nouvelles langues. J’ai créé ce petit site internet pour partager mon enthousiasme des logiciels libres (puis comme je veux devenir enseignant en informatique, ça m’entraine à rédiger des explications claires).


## Remerciements

« La cabane du libre » est un site réalisé avec l’aide de différents outils et ressources libres.

En particulier: 
- le générateur de site statique [hugo](https://gohugo.io/) permet de construire les pages du site
- le site est hébergé sur [framagit](https://framagit.org/public/projects), l’instance gitlab de [framasoft](https://framasoft.org/fr/)
- la texture de planche de fond provient de [polyhaven](https://polyhaven.com/a/wood_planks_dirt).

## Licence

L'intégralité du contenu hébergé sur le site cabanedulibre.frama.io est placé sous licence [CC By-Sa 4.0](http://creativecommons.org/licenses/by-sa/4.0/)  

En gros, ça veut dire que vous avez le droit de copier, partager, ou utiliser comme vous voulez le contenu de ce site, à condition de mentionner l’original et de partager sous les mêmes conditions.

Le code source du site est disponible [ici](https://framagit.org/cabanedulibre/cabanedulibre.frama.io).

